<?php

/**
 * @file
 * Function to return plugin entity row entity view.
 */

/**
 * Class views_navigation_plugin_entity_row_entity_view.
 */
class views_navigation_plugin_entity_row_entity_view extends entity_views_plugin_row_entity_view {

  /**
   * Render callback.
   */
  public function render($values) {
    $return = parent::render($values);
    // For entities such as nodes, HTML containing the link is already built,
    // So the only way we found is to look for the alias in the rendered HTML.
    if (isset($this->view->views_navigation_cid)) {
      module_load_include('inc', 'views_navigation');
      foreach ($this->entities as $entity) {
        _views_navigation_replace_href_in_html($return, $entity, $this->view);
      }
    }
    return $return;
  }

}
