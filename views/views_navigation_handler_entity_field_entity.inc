<?php
/**
 * @file
 */

/**
 * Class views_navigation_handler_entity_field_entity.
 */
class views_navigation_handler_entity_field_entity extends entity_views_handler_field_entity {

  /**
   * Render callback.
   */
  public function render($values) {
    $return = parent::render($values);
    // For entities such as nodes, the HTML containing the link is already
    // built, so that the only way we found is to look for the alias in the
    // rendered HTML...
    if (isset($this->view->views_navigation_cid)) {
      module_load_include('inc', 'views_navigation');
      foreach ($this->wrappers as $wrapper) {
        $entity = $wrapper->value();
        _views_navigation_replace_href_in_html($return, $entity, $this->view);
      }
    }
    return $return;
  }

}
