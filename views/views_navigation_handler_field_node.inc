<?php

/**
 * @file
 * Function to return the Node handler for Views Navigation.
 */

/**
 * Class views_navigation_handler_field_node.
 */
class views_navigation_handler_field_node extends views_handler_field_node {

  /**
   * Implements render_link().
   */
  public function render_link($data, $values) {
    $data = parent::render_link($data, $values);
    if (!empty($this->options['link_to_node'])) {
      module_load_include('inc', 'views_navigation');
      $entity_type = _views_navigation_get_entity_type($this->view->query);
      $entity_id = entity_id($entity_type, $values);
      $this->options['alter']['path'] = _views_navigation_build_url($entity_id, $this->view);
    }
    return $data;
  }

}
