<?php

/**
 * @file
 * Class to implements views navigation handler field field.
 */

/**
 * Class views_navigation_handler_field_field.
 */
class views_navigation_handler_field_field extends views_handler_field_field {

  /**
   * Implements function render_item().
   */
  public function render_item($count, $item) {
    if ($this->options['type'] == 'image' && $this->options['settings']['image_link'] == 'content' && !$this->options['alter']['make_link']) {
      $query = &$item['rendered']['#path']['options']['query'];
      if (!isset($query)) {
        $query = [];
      }
      module_load_include('inc', 'views_navigation');
      $entity_type = $this->base_table;
      $id_key = _views_navigation_get_id_key($entity_type);
      $query = _views_navigation_build_query($item['rendered']['#path']['options']['entity']->$id_key, $this->view, $query);
    }
    return parent::render_item($count, $item);
  }

}
