<?php

/**
 * @file
 * Function to return plugin Display Extender entity view.
 */

/**
 * Class views_navigation_plugin_display_extender_views_navigation.
 */
class views_navigation_plugin_display_extender_views_navigation extends views_plugin_display_extender {

  /**
   * Function to alter the options definition form.
   */
  public function options_definition_alter(&$options) {
    $options['views_navigation'] = [
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    ];

    $options['views_navigation_cycle'] = [
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    ];

    $options['views_navigation_back'] = [
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    ];

    $options['views_navigation_title'] = [
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    ];

    $options['views_navigation_seo_first'] = [
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    ];
  }

  /**
   * Function for return options form.
   */
  public function options_form(&$form, &$form_state) {
    switch ($form_state['section']) {
      case 'views_navigation':
        $form['#title'] .= t('Views navigation');
        $form['views_navigation'] = [
          '#type' => 'checkbox',
          '#title' => t('Add navigation links on entity pages accessed from this view'),
          '#default_value' => $this->display->get_option('views_navigation'),
        ];
        $views_navigation_selector = ':input[name="views_navigation"]';
        $views_navigation_states = ['invisible' => [$views_navigation_selector => ['checked' => FALSE]]];
        $form['views_navigation_cycle'] = [
          '#type' => 'checkbox',
          '#title' => t('Cycling navigation (link to the first on the last entity)'),
          '#default_value' => $this->display->get_option('views_navigation_cycle'),
          '#states' => $views_navigation_states,
        ];
        $form['views_navigation_back'] = [
          '#type' => 'checkbox',
          '#title' => t('Add a back link on entity pages accessed from this view'),
          '#default_value' => $this->display->get_option('views_navigation_back'),
        ];
        $views_navigation_back_selector = ':input[name="views_navigation_back"]';
        $views_navigation_back_states = ['invisible' => [$views_navigation_back_selector => ['checked' => FALSE]]];

        $form['views_navigation_title'] = [
          '#type' => 'checkbox',
          '#title' => t("Use the view's title in back link text"),
          '#default_value' => $this->display->get_option('views_navigation_title'),
          '#states' => $views_navigation_back_states,
        ];
        $form['views_navigation_seo_first'] = [
          '#type' => 'checkbox',
          '#title' => t('Improve SEO at the cost of performance'),
          '#description' => t('This will notably use entity labels for link texts. Can be SQL-greedy, to be avoided for big result sets.'),
          '#default_value' => $this->display->get_option('views_navigation_seo_first'),
          '#states' => $views_navigation_states,
        ];
        break;
    }
  }

  /**
   * Submit callback for the options form.
   */
  public function options_submit(&$form, &$form_state) {
    switch ($form_state['section']) {
      case 'views_navigation':
        $this->display->set_option('views_navigation', $form_state['values']['views_navigation'] ? TRUE : FALSE);
        $this->display->set_option('views_navigation_cycle', $form_state['values']['views_navigation_cycle'] ? TRUE : FALSE);
        $this->display->set_option('views_navigation_back', $form_state['values']['views_navigation_back'] ? TRUE : FALSE);
        $this->display->set_option('views_navigation_title', $form_state['values']['views_navigation_title'] ? TRUE : FALSE);
        $this->display->set_option('views_navigation_seo_first', $form_state['values']['views_navigation_seo_first'] ? TRUE : FALSE);
        break;
    }
  }

  /**
   * Function for options summary.
   */
  public function options_summary(&$categories, &$options) {
    $options['views_navigation'] = [
      'category' => 'other',
      'title' => t('Views navigation'),
      'desc' => t('Add navigation links on entity pages accessed from this view.'),
    ];
    if ($this->display->get_option('views_navigation')) {
      if ($this->display->get_option('views_navigation_cycle')) {
        $value = t('Cycling navigation');
      }
      else {
        $value = t('Linear navigation');
      }
      if ($this->display->get_option('views_navigation_seo_first')) {
        $value .= ', ' . t('SEO first');
      }
      else {
        $value .= ', ' . t('performance first');
      }
      if ($this->display->get_option('views_navigation_back')) {
        $value .= ' + ' . t('Back link');
        if ($this->display->get_option('views_navigation_title')) {
          $value .= t(" with view's title");
        }
      }
    }
    else {
      if ($this->display->get_option('views_navigation_back')) {
        $value = t('Back link');
        if ($this->display->get_option('views_navigation_title')) {
          $value .= t(" with view's title");
        }
      }
      else {
        $value = t('None');
      }
    }

    $options['views_navigation']['value'] = $value;
  }

}
