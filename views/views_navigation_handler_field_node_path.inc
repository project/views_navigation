<?php

/**
 * @file
 * Function to return the Node Path handler for Views Navigation.
 */

/**
 * Class views_navigation_handler_field_node_path.
 */
class views_navigation_handler_field_node_path extends views_handler_field_node_path {

  /**
   * Implements render().
   */
  public function render($values) {
    module_load_include('inc', 'views_navigation');
    return _views_navigation_build_url($this->get_value($values, 'nid'), $this->view, ['absolute' => $this->options['absolute']]);
  }

}
